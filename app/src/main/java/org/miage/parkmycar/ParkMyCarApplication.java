package org.miage.parkmycar;

import android.app.Application;
import android.os.AsyncTask;

import org.miage.parkmycar.activity.ActivityLifecycleHandler;
import org.miage.parkmycar.util.GestionDisponibilite;

public class ParkMyCarApplication extends Application implements ActivityLifecycleHandler.LifecycleListener {
    private static final int FIVE_MINUTES = 60*1000*5;
    private UpdateDispoTask task = null;

    @Override
    public void onCreate() {
        super.onCreate();
        // Register a lifecycle handler to track the foreground/background state
        registerActivityLifecycleCallbacks(new ActivityLifecycleHandler(this));
    }

    @Override
    public void onApplicationStarted() {
    }

    @Override
    public void onApplicationPaused() {
    }

    @Override
    public void onApplicationResumed() {
        if (task == null) {
            task = new UpdateDispoTask();
        }
        task.execute();
    }

    @Override
    public void onApplicationStopped() {
        if (task.getStatus() == AsyncTask.Status.RUNNING) {
            task.cancel(true);
            task = null;
        }
    }

    static class UpdateDispoTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... voids) {
            while(!isCancelled()) {
                try {
                    GestionDisponibilite.addDisponibilite();
                    Thread.sleep(FIVE_MINUTES);
                } catch (Exception e) {
                    System.err.println("Error in UpdateDispoTask : " + e.getMessage());
                }
            }
            return null;
        }
    }
}
