package org.miage.parkmycar.fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.miage.parkmycar.R;
import org.miage.parkmycar.activity.ParkingDetailActivity;
import org.miage.parkmycar.adapter.ParkingAdapter;
import org.miage.parkmycar.dao.ParkingDAO;
import org.miage.parkmycar.model.database.Parking;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


/**
 * Created by samia on 21/03/2018.
 */

public class MapFragment extends Fragment implements OnMapReadyCallback {

    @BindView(R.id.idSearchBar)
    SearchView searchBar;


    Unbinder unbinder;
    GoogleMap mMapParking;
    private List<Parking> allParkings;
    SupportMapFragment mapParkingFragment;
    private Map<String, Parking> mMarkersParkings = new HashMap<>();


    public MapFragment() {
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mMapParking == null) {
            mapParkingFragment.getMapAsync(this);
        } else {
            allParkings = ParkingDAO.getListeParkingDAO();
            mMapParking.clear();
            addMarkers(allParkings);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_map, container, false);

        unbinder = ButterKnife.bind(this, view);

        mapParkingFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.parking_map);

        searchBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchByQuery(query);
                hideKeyboard();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.isEmpty()) {
                    defaultZoomNantes();
                }
                return true;
            }
        });

        return view;
    }

    private void searchByQuery(String newText) {
        final List<Parking> listeParkingRecherche = new ArrayList<>();
        for (final Parking parking : allParkings) {
            if (removeDiacriticalMarks(parking.getNom()).contains(removeDiacriticalMarks(newText))
                    || removeDiacriticalMarks(parking.getAdresse()).contains(removeDiacriticalMarks(newText))
                    || removeDiacriticalMarks(parking.getVille()).contains(removeDiacriticalMarks(newText))
                    || parking.getCodePostal().contains(newText)) {
                listeParkingRecherche.add(parking);
            }
        }
        if (!listeParkingRecherche.isEmpty()) {
            LatLng latLng = new LatLng(listeParkingRecherche.get(0).getLatitude(), listeParkingRecherche.get(0).getLongitude());
            CameraUpdate nantesLocation = CameraUpdateFactory.newLatLngZoom(latLng, 18);
            mMapParking.animateCamera(nantesLocation);
        } else {
            Toast.makeText(getContext(),"Aucun résultat" , Toast.LENGTH_LONG).show();
            defaultZoomNantes();
        }

    }

    private void defaultZoomNantes() {
        // Latitude : 47.2172500°
        // Longitude : -1.5533600°
        LatLng latLng = new LatLng(47.2172500f, -1.5533600f);
        CameraUpdate nantesLocation = CameraUpdateFactory.newLatLngZoom(latLng, 14);
        mMapParking.animateCamera(nantesLocation);
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getActivity().getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(getActivity());
        }
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void addMarkers(List<Parking> mParking) {
        for (Parking parking : mParking) {
            float lat = parking.getLatitude();
            float lng = parking.getLongitude();
            LatLng latLng = new LatLng(lat, lng);
            float markerColor = BitmapDescriptorFactory.HUE_AZURE;
            if (parking.getFavoris()) {
                markerColor = BitmapDescriptorFactory.HUE_ORANGE;
            }

            MarkerOptions markerOptions = new MarkerOptions()
                    .position(latLng)
                    .title(parking.getNom())
                    .snippet(parking.getAdresse() + " " + parking.getCodePostal() + " " + parking.getVille() + " - Places : " + parking.getNombrePlacesDisponibles() + "/" + parking.getNombrePlacesTotal())
                    .icon(BitmapDescriptorFactory.defaultMarker(markerColor));

            Marker marker = mMapParking.addMarker(markerOptions);

            mMarkersParkings.put(marker.getId(), parking);

            LatLngBounds.Builder builder = LatLngBounds.builder();
            builder.include(marker.getPosition());
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMapParking = googleMap;
        mMapParking.getUiSettings().setZoomControlsEnabled(true);
        mMapParking.getUiSettings().setZoomGesturesEnabled(true);
        mMapParking.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        // zoom sur nantes
        defaultZoomNantes();

        allParkings = ParkingDAO.getListeParkingDAO();
        addMarkers(allParkings);

        mMapParking.setOnInfoWindowClickListener(marker -> {
            Parking p = mMarkersParkings.get(marker.getId());
            if (p != null) {
                Intent parkingDetailIntent = new Intent(getActivity(), ParkingDetailActivity.class);
                parkingDetailIntent.putExtra("name",p.getNom());
                parkingDetailIntent.putExtra("address",p.getAdresse());
                parkingDetailIntent.putExtra("codePostal",p.getCodePostal());
                parkingDetailIntent.putExtra("ville",p.getVille());
                parkingDetailIntent.putExtra("accesTransportCommun",p.getAccesTransportCommun());
                parkingDetailIntent.putExtra("description",p.getDescription());
                parkingDetailIntent.putExtra("latitude",p.getLatitude());
                parkingDetailIntent.putExtra("longitude",p.getLongitude());
                parkingDetailIntent.putExtra("nbPlacesDispo",p.getNombrePlacesDisponibles());
                parkingDetailIntent.putExtra("website",p.getSiteWeb());
                parkingDetailIntent.putExtra("ident", p.getIdent().toString());
                startActivity(parkingDetailIntent);
            }
        });
    }

    @OnClick(R.id.idSearchBar)
    public void onclickSearchBarParking() {
        searchBar.setIconified(false);
    }

    // ignorer les accents pour faire des comparaisons lors de la recherche sur le nom et l'adresse
    private String removeDiacriticalMarks(String string) {
        return Normalizer.normalize(string, Normalizer.Form.NFD)
                .replaceAll("[^\\p{ASCII}]", "").toLowerCase();
    }
}