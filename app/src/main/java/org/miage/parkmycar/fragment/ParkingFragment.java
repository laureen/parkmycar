package org.miage.parkmycar.fragment;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListView;
import android.widget.SearchView;

import org.miage.parkmycar.R;
import org.miage.parkmycar.activity.ParkingDetailActivity;
import org.miage.parkmycar.model.database.Parking;
import org.miage.parkmycar.dao.ParkingDAO;
import org.miage.parkmycar.adapter.ParkingAdapter;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class ParkingFragment extends Fragment {

    private String nomParking;
    private String adresse;
    private Boolean carteCB;
    private Boolean carteGR;
    private Boolean espece;
    private int nbrPlaces;

    @BindView(R.id.listViewParking)
    ListView listView;
    Unbinder unbinder;
    @BindView(R.id.searchBarListeParkings)
    SearchView searchBarListeParkings;

    private List<Parking> allParkings;
    private ParkingAdapter parkingAdapter;

    public ParkingFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public ParkingFragment(String nom, String adresse, Boolean carteBancaire, Boolean carteGR, Boolean espece, int nbrPlaces) {
        this.nomParking = nom;
        this.adresse = adresse;
        this.carteCB = carteBancaire;
        this.carteGR = carteGR;
        this.espece = espece;
        this.nbrPlaces = nbrPlaces;
    }

    @Override
    public void onResume() {
        super.onResume();
        allParkings = ParkingDAO.getListeParkingDAO();
        if (allParkings != null) {
            List<Parking> collect = filterListeParking();
            if (parkingAdapter == null) {
                // Instanciate PlaceAdapter
                parkingAdapter = new ParkingAdapter(getContext(), collect);
                listView.setAdapter(parkingAdapter);
            } else {
                parkingAdapter.clear();
                parkingAdapter.addAll(collect);
                parkingAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_parking, container, false);

        unbinder = ButterKnife.bind(this, view);

        searchBarListeParkings.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                hideKeyboard();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchByQuery(newText);
                return true;
            }
        });

        return view;
    }

    private List<Parking> filterListeParking() {
        List<Parking> collect = allParkings;

        if(this.nomParking != null) {
            List<Parking> listeParkingTemp = collect;
            collect = new ArrayList<>();
            for (Parking parking : listeParkingTemp) {
                String comparisonMaterialRechercher = removeDiacriticalMarks(parking.getNom().toLowerCase());
                String comparisonMaterial = removeDiacriticalMarks(this.nomParking.toLowerCase());
                if (comparisonMaterialRechercher.contains(comparisonMaterial)) {
                    collect.add(parking);
                }
            }
        }
        if (this.adresse != null) {
            List<Parking> listeParkingTemp = collect;
            collect = new ArrayList<>();
            for (Parking parking : listeParkingTemp) {
                String comparisonMaterialRechercher = removeDiacriticalMarks(parking.getAdresse().toLowerCase());
                String comparisonMaterial = removeDiacriticalMarks(this.adresse.toLowerCase());
                if (comparisonMaterialRechercher.contains(comparisonMaterial)) {
                    collect.add(parking);
                }
            }
        }
        if (this.espece != null && this.espece) {
            List<Parking> listeParkingTemp = collect;
            collect = new ArrayList<>();
            for (Parking parking : listeParkingTemp) {
                if (parking.getEspece() != null && parking.getEspece().equals(this.espece)) {
                    collect.add(parking);
                }
            }
        }
        if (this.carteGR != null && this.carteGR) {
            List<Parking> listeParkingTemp = collect;
            collect = new ArrayList<>();
            for (Parking parking : listeParkingTemp) {
                if (parking.getCarteGR() != null && parking.getCarteGR().equals(this.carteGR)) {
                    collect.add(parking);
                }
            }
        }
        if (this.carteCB != null && this.carteCB) {
            List<Parking> listeParkingTemp = collect;
            collect = new ArrayList<>();
            for (Parking parking : listeParkingTemp) {
                if (parking.getCarteBancaire() != null && parking.getCarteBancaire().equals(this.carteCB)) {
                    collect.add(parking);
                }
            }
        }

        if (this.nbrPlaces >= 0) {
            List<Parking> listeParkingTemp = collect;
            collect = new ArrayList<>();
            for (Parking parking : listeParkingTemp) {
                if (parking.getNombrePlacesDisponibles() != null && parking.getNombrePlacesDisponibles() >= this.nbrPlaces) {
                    collect.add(parking);
                }
            }
        }
        return collect;
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getActivity().getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(getActivity());
        }
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void searchByQuery(String newText) {
        final List<Parking> listeParkingRecherche = new ArrayList<>();
        for (final Parking parking : allParkings) {
            if (removeDiacriticalMarks(parking.getNom()).contains(removeDiacriticalMarks(newText))
                    || removeDiacriticalMarks(parking.getAdresse()).contains(removeDiacriticalMarks(newText))
                    || removeDiacriticalMarks(parking.getVille()).contains(removeDiacriticalMarks(newText))
                    || parking.getCodePostal().contains(newText)) {
                listeParkingRecherche.add(parking);
            }
        }
        parkingAdapter.clear();
        parkingAdapter.addAll(listeParkingRecherche);
        parkingAdapter.notifyDataSetChanged();
        listView.setAdapter(parkingAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnItemClick(R.id.listViewParking)
    public void onItemSelected(int position){
        Intent parkingDetailIntent = new Intent(getActivity(),ParkingDetailActivity.class);
        Parking p = (Parking) parkingAdapter.getItem(position);
        parkingDetailIntent.putExtra("name",p.getNom());
        parkingDetailIntent.putExtra("address",p.getAdresse());
        parkingDetailIntent.putExtra("codePostal",p.getCodePostal());
        parkingDetailIntent.putExtra("ville",p.getVille());
        parkingDetailIntent.putExtra("accesTransportCommun",p.getAccesTransportCommun());
        parkingDetailIntent.putExtra("description",p.getDescription());
        parkingDetailIntent.putExtra("latitude",p.getLatitude());
        parkingDetailIntent.putExtra("longitude",p.getLongitude());
        parkingDetailIntent.putExtra("nbPlacesDispo",p.getNombrePlacesDisponibles());
        parkingDetailIntent.putExtra("website",p.getSiteWeb());
        parkingDetailIntent.putExtra("ident", p.getIdent().toString());
        startActivity(parkingDetailIntent);
    }

    // ignorer les accents pour faire des comparaisons lors de la recherche sur le nom et l'adresse
    private String removeDiacriticalMarks(String string) {
        return Normalizer.normalize(string, Normalizer.Form.NFD)
                .replaceAll("[^\\p{ASCII}]", "").toLowerCase();
    }

    @OnClick(R.id.searchBarListeParkings)
    public void onclickSearchBarParking() {
        searchBarListeParkings.setIconified(false);
    }
}
