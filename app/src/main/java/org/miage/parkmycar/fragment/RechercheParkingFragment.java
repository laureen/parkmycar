package org.miage.parkmycar.fragment;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import org.miage.parkmycar.R;
import org.miage.parkmycar.dao.ParkingDAO;
import org.miage.parkmycar.model.database.Parking;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class RechercheParkingFragment extends Fragment {


    @BindView(R.id.nomParking)
    EditText nomParking;
    @BindView(R.id.adresseParking)
    EditText adresseParking;
    @BindView(R.id.cartebancaire)
    CheckBox cartebancaire;
    @BindView(R.id.carteGR)
    CheckBox carteGR;
    @BindView(R.id.espece)
    CheckBox espece;
    @BindView(R.id.nbrPlaces)
    SeekBar nbrPlaces;

    Unbinder unbinder;

    @BindView(R.id.idValueSelectSlider)
    TextView idValueSelectSlider;

    @BindView(R.id.idMaxSliderText)
    TextView idMaxSliderText;
    private ChangeFragmentRechercheListener listener;

    @SuppressLint("ValidFragment")
    public RechercheParkingFragment(ChangeFragmentRechercheListener listener) {
        // Required empty public constructor
        this.listener = listener;
    }

    public RechercheParkingFragment() {

    }

    public interface ChangeFragmentRechercheListener {
        void clickSubmitRecherche(String nom, String adresse, Boolean carteBancaire, Boolean carteGR, Boolean espece, int nbrPlaces);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_recherche_parking, container, false);
        unbinder = ButterKnife.bind(this, view);
        List<Parking> allParkings = ParkingDAO.getListeParkingDAO();
        int sliderMax = 0;
        for (Parking parking : allParkings) {
            if (parking.getNombrePlacesDisponibles() != null && parking.getNombrePlacesDisponibles() > sliderMax) {
                sliderMax = parking.getNombrePlacesDisponibles();
            }
        }
        nbrPlaces.setMax(sliderMax);
        idMaxSliderText.setText(String.valueOf(sliderMax));

        nbrPlaces.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                idValueSelectSlider.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick(R.id.buttonSubmit)
    public void clickSubmitRecherche() {
        listener.clickSubmitRecherche(nomParking.getText().toString(), adresseParking.getText().toString(), cartebancaire.isChecked(), carteGR.isChecked(), espece.isChecked(), nbrPlaces.getProgress());
    }


}
