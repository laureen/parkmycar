package org.miage.parkmycar.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import org.miage.parkmycar.R;
import org.miage.parkmycar.activity.ParkingDetailActivity;
import org.miage.parkmycar.dao.ParkingDAO;
import org.miage.parkmycar.model.database.Parking;
import org.miage.parkmycar.adapter.ParkingAdapter;

import java.text.Normalizer;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import butterknife.Unbinder;

public class FavorisFragment extends Fragment {

    Unbinder unbinder;

    @BindView(R.id.listViewParking)
    ListView listView;

    private List<Parking> allParkings;
    private ParkingAdapter parkingAdapter;


    public FavorisFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        allParkings = ParkingDAO.getListeParkingsFavoris();
        if (allParkings != null) {
            if (parkingAdapter == null) {
                // Instanciate PlaceAdapter
                parkingAdapter = new ParkingAdapter(getContext(), allParkings);
                listView.setAdapter(parkingAdapter);
            } else {
                parkingAdapter.clear();
                parkingAdapter.addAll(allParkings);
                parkingAdapter.notifyDataSetChanged();
            }
        }

        if (allParkings != null && allParkings.size() == 0) {
            CharSequence text = "Vous n'avez ajouté aucun parking dans vos favoris";
            int duration = Toast.LENGTH_LONG;
            Toast.makeText(getContext(), text, duration).show();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favoris, container, false);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnItemClick(R.id.listViewParking)
    public void onItemSelected(int position){
        Intent parkingDetailIntent = new Intent(getActivity(),ParkingDetailActivity.class);
        Parking p = (Parking) parkingAdapter.getItem(position);
        if (p != null) {
            parkingDetailIntent.putExtra("name",p.getNom());
            parkingDetailIntent.putExtra("address",p.getAdresse());
            parkingDetailIntent.putExtra("codePostal",p.getCodePostal());
            parkingDetailIntent.putExtra("ville",p.getVille());
            parkingDetailIntent.putExtra("accesTransportCommun",p.getAccesTransportCommun());
            parkingDetailIntent.putExtra("description",p.getDescription());
            parkingDetailIntent.putExtra("latitude",p.getLatitude());
            parkingDetailIntent.putExtra("longitude",p.getLongitude());
            parkingDetailIntent.putExtra("nbPlacesDispo",p.getNombrePlacesDisponibles());
            parkingDetailIntent.putExtra("website",p.getSiteWeb());
            parkingDetailIntent.putExtra("ident", p.getIdent().toString());
            startActivity(parkingDetailIntent);
        }
    }

    // ignorer les accents pour faire des comparaisons lors de la recherche sur le nom et l'adresse
    public static String removeDiacriticalMarks(String string) {
        return Normalizer.normalize(string, Normalizer.Form.NFD)
                .replaceAll("[^\\p{ASCII}]", "");
    }
}
