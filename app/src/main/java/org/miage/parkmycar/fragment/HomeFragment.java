package org.miage.parkmycar.fragment;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.miage.parkmycar.R;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class HomeFragment extends Fragment {

    Unbinder unbinder;
    private ChangeFragmentListener listener;

    @OnClick(R.id.idLayoutCarteParking)
    public void onIdLayoutCarteParkingClicked() {
        listener.clickCarteParkings();
    }

    @OnClick(R.id.idLayoutFavoris)
    public void onIdLayoutFavorisClicked() {
        listener.clickFavorisParkings();
    }

    @OnClick(R.id.idLayoutRecherche)
    public void clickRechercherParkings() {
        listener.clickRechercherParkings();
    }

    public interface ChangeFragmentListener {
        void clickListeParkings();

        void clickCarteParkings();

        void clickFavorisParkings();

        void clickRechercherParkings();
    }

    @SuppressLint("ValidFragment")
    public HomeFragment(ChangeFragmentListener listener) {
        // Required empty public constructor
        this.listener = listener;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);


        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick(R.id.idLayoutListeParkings)
    public void clickListeParkings() {
        listener.clickListeParkings();
    }


}
