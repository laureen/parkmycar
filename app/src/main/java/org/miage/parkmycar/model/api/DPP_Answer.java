package org.miage.parkmycar.model.api;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lloison on 07/03/2018.
 */

public class DPP_Answer {
    @SerializedName("data")
    private DPP_Data DPPData;

    public DPP_Data getDPPData() {
        return DPPData;
    }

    public void setDPPData(DPP_Data DPPData) {
        this.DPPData = DPPData;
    }
}
