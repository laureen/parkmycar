package org.miage.parkmycar.model.database;

import android.net.Uri;

/**
 * Created by lloison on 12/03/2018.
 */

public class User {
    private Uri imageAccount;
    private String nom;
    private String prenom;
    private String email;

    public User() {
    }

    public Uri getImageAccount() {
        return imageAccount;
    }

    public void setImageAccount(Uri imageAccount) {
        this.imageAccount = imageAccount;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
