package org.miage.parkmycar.model.response;

import com.google.gson.annotations.SerializedName;

import org.miage.parkmycar.model.api.LSP_Parking;

import java.util.List;

/**
 * Created by lloison on 07/03/2018.
 */

public class GetListeServiceParkingsPublicsResponse {
    private String version;

    @SerializedName("nb_results")
    private Integer nbrResults;

    @SerializedName("data")
    private List<LSP_Parking> listeAllParkings;

    public GetListeServiceParkingsPublicsResponse() {
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getNbrResults() {
        return nbrResults;
    }

    public void setNbrResults(Integer nbrResults) {
        this.nbrResults = nbrResults;
    }

    public List<LSP_Parking> getListeAllParkings() {
        return listeAllParkings;
    }

    public void setListeAllParkings(List<LSP_Parking> listeAllParkings) {
        this.listeAllParkings = listeAllParkings;
    }
}
