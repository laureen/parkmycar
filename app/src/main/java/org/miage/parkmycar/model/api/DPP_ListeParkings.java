package org.miage.parkmycar.model.api;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by lloison on 09/02/2018.
 */

public class DPP_ListeParkings {
    @SerializedName("Groupe_Parking")
    private List<DPP_Parking> listeDPPParkings;

    public DPP_ListeParkings(List<DPP_Parking> DPPParkings) {
        this.listeDPPParkings = DPPParkings;
    }

    public List<DPP_Parking> getListeDPPParkings() {
        return listeDPPParkings;
    }

    public void setListeDPPParkings(List<DPP_Parking> listeDPPParkings) {
        this.listeDPPParkings = listeDPPParkings;
    }
}
