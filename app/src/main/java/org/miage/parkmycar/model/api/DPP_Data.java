package org.miage.parkmycar.model.api;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lloison on 07/03/2018.
 */

public class DPP_Data {
    @SerializedName("Groupes_Parking")
    private DPP_ListeParkings DPPListeParkings;

    public DPP_Data() {
    }

    public DPP_ListeParkings getDPPListeParkings() {
        return DPPListeParkings;
    }

    public void setDPPListeParkings(DPP_ListeParkings groupe_parking) {
        this.DPPListeParkings = groupe_parking;
    }
}
