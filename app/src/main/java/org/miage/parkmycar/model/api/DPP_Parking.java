package org.miage.parkmycar.model.api;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lloison on 09/02/2018.
 */

public class DPP_Parking {

    @SerializedName("Grp_identifiant")
    private String index;

    @SerializedName("Grp_nom")
    private String nom;

    @SerializedName("Grp_statut")
    private String statut;

    @SerializedName("Grp_pri_aut")
    private String priAut;

    @SerializedName("Grp_disponible")
    private String disponible;

    @SerializedName("Grp_complet")
    private String complet;

    @SerializedName("Grp_exploitation")
    private String exploitation;

    @SerializedName("Grp_horodatage")
    private String horodatage;

    @SerializedName("IdObj")
    private String id;

    public DPP_Parking() {
    }


    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public String getPriAut() {
        return priAut;
    }

    public void setPriAut(String priAut) {
        this.priAut = priAut;
    }

    public String getDisponible() {
        return disponible;
    }

    public void setDisponible(String disponible) {
        this.disponible = disponible;
    }

    public String getComplet() {
        return complet;
    }

    public void setComplet(String complet) {
        this.complet = complet;
    }

    public String getExploitation() {
        return exploitation;
    }

    public void setExploitation(String exploitation) {
        this.exploitation = exploitation;
    }

    public String getHorodatage() {
        return horodatage;
    }

    public void setHorodatage(String horodatage) {
        this.horodatage = horodatage;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
