package org.miage.parkmycar.model.database;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by lloison on 12/03/2018.
 */
@Table(name = "Parkings")
public class Parking  extends Model {

    @Column(name = "Ident", index = true, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    private Long ident;

    @Column(name = "Nom")
    private String nom;

    @Column(name = "NombrePlacesDisponibles")
    private Integer nombrePlacesDisponibles;

    @Column(name = "NombrePlacesTotal")
    private Integer nombrePlacesTotal;

    @Column(name = "InfosComplementaires")
    private String infosComplementaires;

    @Column(name = "Adresse")
    private String adresse;

    @Column(name = "CodePostal")
    private String codePostal;

    @Column(name = "Ville")
    private String ville;

    @Column(name = "AccesTransportCommun")
    private String accesTransportCommun;

    @Column(name = "Categorie")
    private String categorie;

    @Column(name = "Type")
    private String type;

    @Column(name = "SiteWeb")
    private String siteWeb;

    @Column(name = "Telephone")
    private String telephone;

    @Column(name = "Description")
    private String description;

    @Column(name = "CarteBancaire")
    private Boolean carteBancaire;

    @Column(name = "Espece")
    private Boolean espece;

    @Column(name = "CarteGR")
    private Boolean carteGR;

    @Column(name = "Velo")
    private Boolean velo;

    @Column(name = "Voiture")
    private Boolean voiture;

    @Column(name = "Moto")
    private Boolean moto;

    @Column(name = "Electrique")
    private Boolean electrique;

    @Column(name = "MobiliteReduite")
    private Boolean mobiliteReduite;

    @Column(name = "Longitude")
    private Float longitude;

    @Column(name = "Latitude")
    private Float latitude;

    @Column(name = "Favoris")
    private Boolean favoris;

    public Parking() {
        super();
    }

    public Long getIdent() {
        return ident;
    }

    public void setIdent(Long ident) {
        this.ident = ident;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Integer getNombrePlacesDisponibles() {
        return nombrePlacesDisponibles;
    }

    public void setNombrePlacesDisponibles(Integer nombrePlacesDisponibles) {
        this.nombrePlacesDisponibles = nombrePlacesDisponibles;
    }

    public Integer getNombrePlacesTotal() {
        return nombrePlacesTotal;
    }

    public void setNombrePlacesTotal(Integer nombrePlacesTotal) {
        this.nombrePlacesTotal = nombrePlacesTotal;
    }

    public String getInfosComplementaires() {
        return infosComplementaires;
    }

    public void setInfosComplementaires(String infosComplementaires) {
        this.infosComplementaires = infosComplementaires;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getAccesTransportCommun() {
        return accesTransportCommun;
    }

    public void setAccesTransportCommun(String accesTransportCommun) {
        this.accesTransportCommun = accesTransportCommun;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSiteWeb() {
        return siteWeb;
    }

    public void setSiteWeb(String siteWeb) {
        this.siteWeb = siteWeb;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getCarteBancaire() {
        return carteBancaire;
    }

    public void setCarteBancaire(Boolean carteBancaire) {
        this.carteBancaire = carteBancaire;
    }

    public Boolean getEspece() {
        return espece;
    }

    public void setEspece(Boolean espece) {
        this.espece = espece;
    }

    public Boolean getCarteGR() {
        return carteGR;
    }

    public void setCarteGR(Boolean carteGR) {
        this.carteGR = carteGR;
    }

    public Boolean getVelo() {
        return velo;
    }

    public void setVelo(Boolean velo) {
        this.velo = velo;
    }

    public Boolean getVoiture() {
        return voiture;
    }

    public void setVoiture(Boolean voiture) {
        this.voiture = voiture;
    }

    public Boolean getMoto() {
        return moto;
    }

    public void setMoto(Boolean moto) {
        this.moto = moto;
    }

    public Boolean getElectrique() {
        return electrique;
    }

    public void setElectrique(Boolean electrique) {
        this.electrique = electrique;
    }

    public Boolean getMobiliteReduite() {
        return mobiliteReduite;
    }

    public void setMobiliteReduite(Boolean mobiliteReduite) {
        this.mobiliteReduite = mobiliteReduite;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Boolean getFavoris() {
        return favoris;
    }

    public void setFavoris(Boolean favoris) {
        this.favoris = favoris;
    }
}
