package org.miage.parkmycar.model.api;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lloison on 09/02/2018.
 */

public class LSP_Parking {

    @SerializedName("_IDOBJ")
    private Long id;

    @SerializedName("SERVICE_VELO")
    private String serviceVelo;

    @SerializedName("CAPACITE_VOITURE")
    private Long capaciteVoiture;

    @SerializedName("STATIONNEMENT_VELO")
    private String stationnementVelo;

    @SerializedName("INFOS_COMPLEMENTAIRES")
    private String infosComplementaires;

    @SerializedName("STATIONNEMENT_VELO_SECURISE")
    private String stationnementVeloSecurise;

    @SerializedName("geo")
    private LSP_GeoLocation localisationName;

    @SerializedName("_l")
    private Float[] coordonneesGPS;

    @SerializedName("CAPACITE_VEHICULE_ELECTRIQUE")
    private Integer capaciteVehiculeElectrique;

    @SerializedName("CODE_POSTAL")
    private Integer codePostal;

    @SerializedName("ACCES_PMR")
    private String accesMobiliteReduite;

    @SerializedName("TELEPHONE")
    private String telephone;

    @SerializedName("MOYEN_PAIEMENT")
    private String moyenPaiement;

    @SerializedName("ACCES_TRANSPORTS_COMMUNS")
    private String accesTransportEnCommun;

    @SerializedName("CAPACITE_MOTO")
    private Integer capaciteMoto;

    @SerializedName("CONDITIONS_D_ACCES")
    private String conditionAcces;

    @SerializedName("COMMUNE")
    private String commune;

    @SerializedName("CAPACITE_VELO")
    private Integer capaciteVelo;

    @SerializedName("PRESENTATION")
    private String presentation;

    @SerializedName("CAPACITE_PMR")
    private Integer capaciteMobiliteReduite;

    @SerializedName("LIBCATEGORIE")
    private String categorie;

    @SerializedName("SITE_WEB")
    private String siteWeb;

    @SerializedName("ADRESSE")
    private String adresse;

    @SerializedName("LIBTYPE")
    private String type;


    public LSP_Parking() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getServiceVelo() {
        return serviceVelo;
    }

    public void setServiceVelo(String serviceVelo) {
        this.serviceVelo = serviceVelo;
    }

    public Long getCapaciteVoiture() {
        return capaciteVoiture;
    }

    public void setCapaciteVoiture(Long capaciteVoiture) {
        this.capaciteVoiture = capaciteVoiture;
    }

    public String getStationnementVelo() {
        return stationnementVelo;
    }

    public void setStationnementVelo(String stationnementVelo) {
        this.stationnementVelo = stationnementVelo;
    }

    public String getInfosComplementaires() {
        return infosComplementaires;
    }

    public void setInfosComplementaires(String infosComplementaires) {
        this.infosComplementaires = infosComplementaires;
    }

    public String getStationnementVeloSecurise() {
        return stationnementVeloSecurise;
    }

    public void setStationnementVeloSecurise(String stationnementVeloSecurise) {
        this.stationnementVeloSecurise = stationnementVeloSecurise;
    }

    public LSP_GeoLocation getLocalisationName() {
        return localisationName;
    }

    public void setLocalisationName(LSP_GeoLocation localisationName) {
        this.localisationName = localisationName;
    }

    public Float[] getCoordonneesGPS() {
        return coordonneesGPS;
    }

    public void setCoordonneesGPS(Float[] coordonneesGPS) {
        this.coordonneesGPS = coordonneesGPS;
    }

    public Integer getCapaciteVehiculeElectrique() {
        return capaciteVehiculeElectrique;
    }

    public void setCapaciteVehiculeElectrique(Integer capaciteVehiculeElectrique) {
        this.capaciteVehiculeElectrique = capaciteVehiculeElectrique;
    }

    public Integer getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(Integer codePostal) {
        this.codePostal = codePostal;
    }

    public String getAccesMobiliteReduite() {
        return accesMobiliteReduite;
    }

    public void setAccesMobiliteReduite(String accesMobiliteReduite) {
        this.accesMobiliteReduite = accesMobiliteReduite;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getMoyenPaiement() {
        return moyenPaiement;
    }

    public void setMoyenPaiement(String moyenPaiement) {
        this.moyenPaiement = moyenPaiement;
    }

    public String getAccesTransportEnCommun() {
        return accesTransportEnCommun;
    }

    public void setAccesTransportEnCommun(String accesTransportEnCommun) {
        this.accesTransportEnCommun = accesTransportEnCommun;
    }

    public Integer getCapaciteMoto() {
        return capaciteMoto;
    }

    public void setCapaciteMoto(Integer capaciteMoto) {
        this.capaciteMoto = capaciteMoto;
    }

    public String getConditionAcces() {
        return conditionAcces;
    }

    public void setConditionAcces(String conditionAcces) {
        this.conditionAcces = conditionAcces;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public Integer getCapaciteVelo() {
        return capaciteVelo;
    }

    public void setCapaciteVelo(Integer capaciteVelo) {
        this.capaciteVelo = capaciteVelo;
    }

    public String getPresentation() {
        return presentation;
    }

    public void setPresentation(String presentation) {
        this.presentation = presentation;
    }

    public Integer getCapaciteMobiliteReduite() {
        return capaciteMobiliteReduite;
    }

    public void setCapaciteMobiliteReduite(Integer capaciteMobiliteReduite) {
        this.capaciteMobiliteReduite = capaciteMobiliteReduite;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public String getSiteWeb() {
        return siteWeb;
    }

    public void setSiteWeb(String siteWeb) {
        this.siteWeb = siteWeb;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
