package org.miage.parkmycar.model.api;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lloison on 07/03/2018.
 */

public class DPP_Opendata {

    private String request;

    @SerializedName("answer")
    private DPP_Answer DPPAnswer;

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public DPP_Answer getDPPAnswer() {
        return DPPAnswer;
    }

    public void setDPPAnswer(DPP_Answer DPPAnswer) {
        this.DPPAnswer = DPPAnswer;
    }
}
