package org.miage.parkmycar.model.api;

/**
 * Created by lloison on 09/02/2018.
 */

public class LSP_GeoLocation {
    private String name;

    public LSP_GeoLocation() {
    }

    public LSP_GeoLocation(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
