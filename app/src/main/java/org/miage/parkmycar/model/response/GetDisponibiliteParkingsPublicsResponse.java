package org.miage.parkmycar.model.response;

import com.google.gson.annotations.SerializedName;

import org.miage.parkmycar.model.api.DPP_Opendata;

/**
 * Created by lloison on 07/03/2018.
 */

public class GetDisponibiliteParkingsPublicsResponse {
    @SerializedName("opendata")
    private DPP_Opendata DPPOpendata;

    public GetDisponibiliteParkingsPublicsResponse(DPP_Opendata DPPOpendata) {
        this.DPPOpendata = DPPOpendata;
    }

    public GetDisponibiliteParkingsPublicsResponse() {
    }

    public DPP_Opendata getDPPOpendata() {
        return DPPOpendata;
    }

    public void setDPPOpendata(DPP_Opendata DPPOpendata) {
        this.DPPOpendata = DPPOpendata;
    }
}
