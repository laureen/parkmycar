package org.miage.parkmycar.dao;

import com.activeandroid.query.Select;
import com.activeandroid.query.Update;

import org.miage.parkmycar.model.database.Parking;

import java.util.List;

/**
 * Created by lloison on 12/03/2018.
 */

public class ParkingDAO {

    public static List<Parking> getListeParkingDAO() {
        return new Select().from(Parking.class).execute();
    }

    public static void updateDisponibiiteByIdParking(Long id, Integer disponibilite) {
        new Update(Parking.class)
                .set("NombrePlacesDisponibles = ?", disponibilite)
                .where("Ident = ?", id)
                .execute();
    }

    public static List<Parking> getListeParkingsFavoris() {
        return new Select().from(Parking.class).where("Favoris = ?", 1).execute();
    }

    public static void addParkingToFavorite(Long idParking) {
        new Update(Parking.class)
                .set("Favoris = ?", 1)
                .where("Ident = ?", idParking)
                .execute();
    }

    public static void removeParkingToFavorite(Long idParking) {
        new Update(Parking.class)
                .set("Favoris = ?", 0)
                .where("Ident = ?", idParking)
                .execute();
    }

    public static Parking getParkingById(Long id) {
        return new Select().from(Parking.class).where("Ident = ?", id).executeSingle();
    }
}
