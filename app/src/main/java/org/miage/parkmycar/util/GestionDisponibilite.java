package org.miage.parkmycar.util;

import org.miage.parkmycar.dao.ParkingDAO;
import org.miage.parkmycar.model.api.DPP_Parking;
import org.miage.parkmycar.model.database.Parking;
import org.miage.parkmycar.model.response.GetDisponibiliteParkingsPublicsResponse;
import org.miage.parkmycar.service.ApiService;
import org.miage.parkmycar.service.ParkingService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GestionDisponibilite {

    public static void addDisponibilite() {
        List<Parking> listeParkingDAO = ParkingDAO.getListeParkingDAO();

        if (!listeParkingDAO.isEmpty()) {
            ParkingService parkingService = ApiService.getClient().create(ParkingService.class);
            Call<GetDisponibiliteParkingsPublicsResponse> callDPP = parkingService.getDisponibiliteParkingsPublics();

            callDPP.enqueue(new Callback<GetDisponibiliteParkingsPublicsResponse>() {
                @Override
                public void onResponse(Call<GetDisponibiliteParkingsPublicsResponse> call, Response<GetDisponibiliteParkingsPublicsResponse> response) {
                    GetDisponibiliteParkingsPublicsResponse results = response.body();
                    if (results != null) {
                        List<DPP_Parking> allDPParking = results.getDPPOpendata().getDPPAnswer().getDPPData().getDPPListeParkings().getListeDPPParkings();

                        // récupération des places disponbiles pour chaque parking
                        for (DPP_Parking parkingDPP : allDPParking) {
                            ParkingDAO.updateDisponibiiteByIdParking(Long.valueOf(parkingDPP.getId()), Integer.valueOf(parkingDPP.getDisponible()));
                        }
                        System.out.println("**************** UPDATE DISPO ********************");
                    }
                }

                @Override
                public void onFailure(Call<GetDisponibiliteParkingsPublicsResponse> call, Throwable t) {

                }
            });
        }
    }
}
