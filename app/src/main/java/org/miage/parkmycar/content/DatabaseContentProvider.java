package org.miage.parkmycar.content;


import com.activeandroid.Configuration;
import com.activeandroid.content.ContentProvider;

import org.miage.parkmycar.model.database.Parking;

public class DatabaseContentProvider extends ContentProvider {
    @Override
    protected Configuration getConfiguration() {
        Configuration.Builder builder = new Configuration.Builder(getContext());
        builder.addModelClass(Parking.class);
        return builder.create();
    }
}
