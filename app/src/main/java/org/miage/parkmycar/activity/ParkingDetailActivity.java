package org.miage.parkmycar.activity;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.SupportStreetViewPanoramaFragment;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Picasso;

import org.miage.parkmycar.R;
import org.miage.parkmycar.dao.ParkingDAO;
import org.miage.parkmycar.model.database.Parking;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by samia on 12/03/2018.
 */

public class ParkingDetailActivity extends AppCompatActivity {

    @BindView(R.id.parking_address)
    TextView parkingAddressTextView;

    @BindView(R.id.parking_name)
    TextView parkingNameTextView;

    @BindView(R.id.parking_website)
    TextView parkingWebsiteTextView;

    @BindView(R.id.parking_description)
    TextView parkingDescTextView;

    @BindView(R.id.parking_name_layout)
    LinearLayout imageParkName;

    @BindView(R.id.parking_loc_layout)
    LinearLayout imageParkLoc;

    @BindView(R.id.parking_siteweb_layout)
    LinearLayout imageParkSite;

    @BindView(R.id.parking_info_layout)
    LinearLayout imageParkInfo;

    @BindView(R.id.parking_favorite_button)
    FloatingActionButton buttonFavoris;

    private String pName,pAddress,pCP,pVille,pWebsite,pDescription;
    private Float pLat,pLng;
    private Integer pNbPlacesDispo;
    private Long pIdent;
    private Parking parkingById;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        ButterKnife.bind(this);

        Intent parkingDetailIntent = getIntent();
        pIdent = Long.valueOf(parkingDetailIntent.getStringExtra("ident"));
        parkingById = ParkingDAO.getParkingById(pIdent);
        if (parkingById.getFavoris()) {
            buttonFavoris.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorIconFavoris)));
        } else {
            buttonFavoris.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorDefaultButton)));
        }
        pName = parkingDetailIntent.getStringExtra("name");
        if(pName == null || pName.equals("")){
            imageParkName.setVisibility(View.GONE);
        }
        parkingNameTextView.setText(pName);

        pAddress = parkingDetailIntent.getStringExtra("address");
        pCP = parkingDetailIntent.getStringExtra("codePostal");
        pVille = parkingDetailIntent.getStringExtra("ville");

        if(pAddress == null || pAddress.equals("")){
            imageParkLoc.setVisibility(View.GONE);
        }
        String adresseComplete = pAddress + "\n" + pCP + " " + pVille;
        parkingAddressTextView.setText(adresseComplete);


        pDescription = "";
        if (parkingById.getDescription() != null) {
            pDescription += "Description : " + parkingById.getDescription();
        }
        if (parkingById.getTelephone() != null) {
            pDescription += "\nNuméro de téléphone : " + parkingById.getTelephone();
        }

        if (parkingById.getInfosComplementaires() != null) {
            pDescription += "\nInformations complémentaires : " + parkingById.getInfosComplementaires();
        }
        if(pDescription.equals("")){
            imageParkInfo.setVisibility(View.GONE);
        }
        parkingDescTextView.setText(pDescription);

        pLat = parkingDetailIntent.getFloatExtra("latitude",0);
        pLng = parkingDetailIntent.getFloatExtra("longitude",0);

        pNbPlacesDispo = parkingDetailIntent.getIntExtra("nbPlacesDispo",0);
        pWebsite = parkingDetailIntent.getStringExtra("website");
        if(pWebsite == null || pWebsite.equals("")){
            imageParkSite.setVisibility(View.GONE);
        }
        String s = pWebsite + "\n" + pNbPlacesDispo + " places disponibles";
        parkingWebsiteTextView.setText(s);

        //Afficher streetview du parking
        SupportStreetViewPanoramaFragment streetViewPanoramaFragment =
                (SupportStreetViewPanoramaFragment)
                        getSupportFragmentManager().findFragmentById(R.id.streetviewpanorama);
        streetViewPanoramaFragment.getStreetViewPanoramaAsync(
                new OnStreetViewPanoramaReadyCallback() {
                    @Override
                    public void onStreetViewPanoramaReady(StreetViewPanorama panorama) {
                        // Only set the panorama to SYDNEY on startup (when no panoramas have been
                        // loaded which is when the savedInstanceState is null).
                        if (savedInstanceState == null) {
                            panorama.setPosition(new LatLng(pLat,pLng));


                        }
                    }
                });

    }

    @OnClick(R.id.parking_favorite_button)
    public void saveFavoritePaking() {
        if (parkingById.getFavoris()) {
            ParkingDAO.removeParkingToFavorite(pIdent);
            buttonFavoris.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorDefaultButton)));
        } else {
            ParkingDAO.addParkingToFavorite(pIdent);
            buttonFavoris.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorIconFavoris)));
        }
    }

    @OnClick(R.id.parking_share_button)
    public void shareActionParking(){
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,pName);
        shareIntent.putExtra(Intent.EXTRA_TEXT, "Application ParkMyCar, "+pName+" à l'adresse "+pAddress+".\n"+pDescription +"\nIci, le lien pour plus d'infos: "+pWebsite);
        shareIntent.setType("text/plain");
        startActivity(Intent.createChooser(shareIntent,"Partager via"));
    }
}