package org.miage.parkmycar.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.squareup.picasso.Picasso;

import org.miage.parkmycar.R;
import org.miage.parkmycar.fragment.FavorisFragment;
import org.miage.parkmycar.fragment.HomeFragment;
import org.miage.parkmycar.fragment.MapFragment;
import org.miage.parkmycar.fragment.ParkingFragment;
import org.miage.parkmycar.fragment.RechercheParkingFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, HomeFragment.ChangeFragmentListener, RechercheParkingFragment.ChangeFragmentRechercheListener {

    @BindView(R.id.fragmentContainer)
    FrameLayout fragmentContainer;

    private FragmentManager fragmentManager;

    private GoogleSignInClient mGoogleSignInClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // ajout butterknife
        ButterKnife.bind(this);

        // instanciation du fragment
        fragmentManager = getSupportFragmentManager();

        // instanciation du google sign
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerLayout = navigationView.getHeaderView(0);

        // récupération du display name du compte connecté
        String accountDisplayName = getIntent().getStringExtra("accountDisplayName");
        TextView mtextViewUser = headerLayout.findViewById(R.id.id_user_connected);
        mtextViewUser.setText(accountDisplayName);

        // récupération de la photo du compte connecté
        String accountUriPhoto = getIntent().getStringExtra("accountUriPhotoAccount");

        ImageView mimageViewUser = headerLayout.findViewById(R.id.imageView);
        if (!("").equals(accountUriPhoto) && accountUriPhoto != null) {
            Picasso.with(this).load(accountUriPhoto).resize(95, 95).into(mimageViewUser);
        } else {
            Picasso.with(this).load("https://genglobal.org/sites/default/files/pictures/default-user-image.png").resize(130, 130).into(mimageViewUser);
        }

        // instanciation du premier fragment lors du lancement de l'activity
        addFragment(new HomeFragment(this));

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_info:
                Intent i = new Intent(this,AboutActivity.class);
                startActivity(i);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = this.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(this);
        }
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        hideKeyboard();
        if (id == R.id.nav_carte) {
            // Handle the camera action
            clickCarteParkings();
        } else if (id == R.id.nav_favoris) {
            clickFavorisParkings();
        } else if (id == R.id.nav_rechercher) {
            clickRechercherParkings();
        } else if (id == R.id.nav_liste) {
            clickListeParkings();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void updateUI() {
        Intent i = new Intent(this, ConnexionActivity.class);
        startActivity(i);
        finish();
    }

    private void addFragment(@NonNull Fragment fragment) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(fragmentContainer.getId(), fragment);
        fragmentTransaction.disallowAddToBackStack();
        fragmentTransaction.commit();
    }

    private void replaceFragment(@NonNull Fragment fragment) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(fragmentContainer.getId(), fragment);
        fragmentTransaction.addToBackStack(MainActivity.class.getSimpleName());
        fragmentTransaction.commit();
    }

    @Override
    public void clickListeParkings() {
        replaceFragment(new ParkingFragment());
    }

    @Override
    public void clickCarteParkings() {
        replaceFragment(new MapFragment());
    }

    @Override
    public void clickFavorisParkings() {
        replaceFragment(new FavorisFragment());
    }

    @Override
    public void clickRechercherParkings() {
        replaceFragment(new RechercheParkingFragment(this));
    }

    @Override
    public void clickSubmitRecherche(String nom, String adresse, Boolean carteBancaire, Boolean cheque, Boolean espece, int nbrPlaces) {
        replaceFragment(new ParkingFragment(nom, adresse, carteBancaire, cheque, espece, nbrPlaces));
    }

    @OnClick(R.id.nav_deconnexion)
    public void clickDeconnexion() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        updateUI();
                    }
                });
    }

}
