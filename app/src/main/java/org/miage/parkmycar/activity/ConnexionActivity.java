package org.miage.parkmycar.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import org.miage.parkmycar.R;
import org.miage.parkmycar.dao.ParkingDAO;
import org.miage.parkmycar.model.api.DPP_Parking;
import org.miage.parkmycar.model.api.LSP_Parking;
import org.miage.parkmycar.model.database.Parking;
import org.miage.parkmycar.model.response.GetDisponibiliteParkingsPublicsResponse;
import org.miage.parkmycar.model.response.GetListeServiceParkingsPublicsResponse;
import org.miage.parkmycar.service.ApiService;
import org.miage.parkmycar.service.ParkingService;
import org.miage.parkmycar.util.GestionDisponibilite;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConnexionActivity extends AppCompatActivity {

    private static final String TAG = "Error";
    private static final int RC_SIGN_IN = 1;
    private GoogleSignInClient mGoogleSignInClient;

    private Call<GetListeServiceParkingsPublicsResponse> callLSP;
    private Call<GetDisponibiliteParkingsPublicsResponse> callDPP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connexion);
        ButterKnife.bind(this);

        Configuration.Builder configurationBuilder = new Configuration.Builder(this);
        configurationBuilder.addModelClass(Parking.class);
        ActiveAndroid.initialize(configurationBuilder.create());

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Check for existing Google Sign In account, if the user is already signed in
        // the GoogleSignInAccount will be non-null.
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        if (account != null) {
            updateUI(account);
        }
    }

    @OnClick(R.id.sign_in_button)
    public void googleSignInClient() {
        signIn();
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this callLSP is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
        }
    }

    private void updateUI(GoogleSignInAccount account) {
        //récupération des données des API nantes métropoles et ajout en base SQLite
        if (ParkingDAO.getListeParkingDAO().size() == 0) {
            getAllParkingsNantes();
        }
        Intent i = new Intent(this, MainActivity.class);
        i.putExtra("accountDisplayName", account.getDisplayName());
        if (account.getPhotoUrl() != null) {
            i.putExtra("accountUriPhotoAccount", account.getPhotoUrl().toString());
        } else {
            i.putExtra("accountUriPhotoAccount", "");
        }
        startActivity(i);
        finish();
    }

    private void getAllParkingsNantes() {
        ParkingService parkingService = ApiService.getClient().create(ParkingService.class);
        callLSP = parkingService.getListeServiceParkingsPublics();
        callDPP = parkingService.getDisponibiliteParkingsPublics();

        executeListeAllParkings();
    }

    private void executeListeAllParkings() {
        callLSP.enqueue(new Callback<GetListeServiceParkingsPublicsResponse>() {
            @Override
            public void onResponse(Call<GetListeServiceParkingsPublicsResponse> call, Response<GetListeServiceParkingsPublicsResponse> response) {
                GetListeServiceParkingsPublicsResponse results = response.body();
                if (results != null) {
                    List<LSP_Parking> allLSPParking = results.getListeAllParkings();
                    firstSaveAllParkings(allLSPParking);
                    GestionDisponibilite.addDisponibilite();
                }
            }

            @Override
            public void onFailure(Call<GetListeServiceParkingsPublicsResponse> call, Throwable t) {

            }
        });
    }

    private void firstSaveAllParkings(List<LSP_Parking> allLSPParking) {
        for (LSP_Parking parkingLSP: allLSPParking) {
            Parking parking = new Parking();
            parking.setIdent(parkingLSP.getId());
            parking.setNom(parkingLSP.getLocalisationName().getName());
            parking.setAdresse(parkingLSP.getAdresse());
            parking.setCodePostal(parkingLSP.getCodePostal().toString());
            parking.setVille(parkingLSP.getCommune());
            parking.setAccesTransportCommun(parkingLSP.getAccesTransportEnCommun());
            parking.setDescription(parkingLSP.getPresentation());
            parking.setInfosComplementaires(parkingLSP.getInfosComplementaires());
            parking.setLatitude(parkingLSP.getCoordonneesGPS()[0]);
            parking.setLongitude(parkingLSP.getCoordonneesGPS()[1]);
            parking.setSiteWeb(parkingLSP.getSiteWeb());
            parking.setTelephone(parkingLSP.getTelephone());
            parking.setVoiture(parkingLSP.getCapaciteVoiture() != null && parkingLSP.getCapaciteVoiture() > 0);
            parking.setVelo(parkingLSP.getCapaciteVelo() != null && parkingLSP.getCapaciteVelo() > 0);
            parking.setMobiliteReduite(parkingLSP.getCapaciteMobiliteReduite() != null && parkingLSP.getCapaciteMobiliteReduite() > 0);
            parking.setMoto(parkingLSP.getCapaciteMoto() != null && parkingLSP.getCapaciteMoto() > 0);
            parking.setElectrique(parkingLSP.getCapaciteVehiculeElectrique() != null && parkingLSP.getCapaciteVehiculeElectrique() > 0);
            parking.setNombrePlacesTotal(parkingLSP.getCapaciteVoiture().intValue());
            parking.setFavoris(false);

            // détermination des moyens de paiement
            if (parkingLSP.getMoyenPaiement() != null) {
                String[] split = parkingLSP.getMoyenPaiement().split(",");
                for(int i = 0; i < split.length; i++) {
                    if (i==0) {
                        parking.setCarteBancaire(split[0] != null && split[0].equals("CB en borne de sortie"));
                    }
                    if (i==1) {
                        parking.setEspece(split[1] != null && split[1].equals(" Espèces"));
                    }
                    if (i==2) {
                        parking.setCarteGR(split[2] != null && split[2].equals(" Total GR"));
                    }
                }
            }

            parking.save();
        }
    }

}
