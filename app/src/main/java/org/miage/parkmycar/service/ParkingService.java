package org.miage.parkmycar.service;


import org.miage.parkmycar.model.response.GetDisponibiliteParkingsPublicsResponse;
import org.miage.parkmycar.model.response.GetListeServiceParkingsPublicsResponse;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by lloison on 09/02/2018.
 */

public interface ParkingService {

    @GET("getDisponibiliteParkingsPublics/1.0/G8LJW4GDXINUVU2/?output=json")
    Call<GetDisponibiliteParkingsPublicsResponse> getDisponibiliteParkingsPublics();

    @GET("publication/24440040400129_NM_NM_00044/LISTE_SERVICES_PKGS_PUB_NM_STBL/content/?format=json")
    Call<GetListeServiceParkingsPublicsResponse> getListeServiceParkingsPublics();

}
