package org.miage.parkmycar.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import org.miage.parkmycar.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by samia on 28/04/2018.
 */

public class ExpandableListAdapter extends BaseExpandableListAdapter {
    private Context context;
    private List<String> mParentItem;
    private HashMap<String, List<String>> mChildItem;

    @Nullable
    @BindView(R.id.listTitle)
    TextView listTitleTextView;

    @Nullable
    @BindView(R.id.expandedListItem)
    TextView itemTextView;

    public ExpandableListAdapter(Context context, List<String> ParentItem,
                                 HashMap<String, List<String>> ChildItem) {
        this.context = context;
        this.mParentItem = ParentItem;
        this.mChildItem = ChildItem;
    }


    public static HashMap<String, List<String>> getData() {
        HashMap<String, List<String>> ParentItem = new HashMap<String, List<String>>();

        List<String> list1 = new ArrayList<String>();
        list1.add("Application qui vous permet de connaître les parkings de la ville de Nantes, la capacité et le nombre de places disponibles en temps réel. ");
        list1.add("Source des données utilisées: \n- L'application utilise les API Open Data de Nantes Métropole.");

        List<String> list2 = new ArrayList<String>();
        list2.add("Oui. L'application est gratuite.");
        list2.add("Elle fonctionne en mode Hors-ligne et WIFI.");

        List<String> list3 = new ArrayList<String>();
        list3.add("Tout le monde.");

        List<String> list4 = new ArrayList<String>();
        list4.add("L'application est développée par deux étudiantes de la MIAGE de Nantes.");
        list4.add("Il s'agit de la version 1 de l'application.");

        ParentItem.put("Qu'est-ce que l'application mobile ParkMyCar ?",list1);
        ParentItem.put("L'application est-elle gratuite ?",list2);
        ParentItem.put("Qui peut utiliser l'application ?",list3);
        ParentItem.put("Plus d'informations", list4);

        return ParentItem;

    }

    @Override
    public int getGroupCount() {
        return this.mParentItem.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return this.mChildItem.get(this.mParentItem.get(i))
                .size();
    }

    @Override
    public Object getGroup(int i) {
        return this.mParentItem.get(i);
    }

    @Override
    public Object getChild(int i, int i1) {
        return this.mChildItem.get(this.mParentItem.get(i)).get(i1);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }


    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        View actualView = view;
        String listTitle = (String) getGroup(i);
        if (actualView == null) {
            LayoutInflater
                    layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            actualView = layoutInflater.inflate(R.layout.list_group_about, viewGroup,false);
        }

        ButterKnife.bind(this, actualView);
        listTitleTextView.setTypeface(null, Typeface.BOLD);
        listTitleTextView.setText(listTitle);

        return actualView;
    }

    @Override
    public View getChildView(int i, int i1, boolean b,View view,ViewGroup viewGroup) {
        View actualView = view;
        String expandedListText = (String) getChild(i,i1);
        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            actualView = layoutInflater.inflate(R.layout.list_item_about, viewGroup,false);
        }
        ButterKnife.bind(this, actualView);
        itemTextView.setText(expandedListText);

        return actualView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }


}
