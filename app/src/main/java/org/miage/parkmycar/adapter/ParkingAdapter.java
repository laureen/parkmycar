package org.miage.parkmycar.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.miage.parkmycar.R;
import org.miage.parkmycar.model.database.Parking;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by samia on 18/02/2018.
 */

public class ParkingAdapter extends ArrayAdapter<Parking> {


    @BindView(R.id.parking_name)
    TextView parkingNameTextView;

    @BindView(R.id.parking_address)
    TextView parkingAddressTextView;

    @BindView(R.id.imageCarteBancaire)
    LinearLayout imageCarteBancaire;

    @BindView(R.id.imageEspece)
    LinearLayout imageEspece;

    @BindView(R.id.imageCarteGR)
    LinearLayout imageCarteGR;

    @BindView(R.id.nbrPlaces)
    TextView nbrPlaces;

    @BindView(R.id.iconFavoriteParking)
    ImageView iconFavoriteParking;

    public ParkingAdapter(Context context, final List<Parking> allParkings) {
        super(context, -1, allParkings);
    }

    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View actualView = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            actualView = inflater.inflate(R.layout.parking_adapter, parent, false);
        }
        ButterKnife.bind(this, actualView);

        Parking item = getItem(position);
        if (item != null) {
            String localisationName = item.getNom() != null ? item.getNom() : "";
            parkingNameTextView.setText(localisationName);

            String adresse = item.getAdresse() != null ? item.getAdresse() : "";
            parkingAddressTextView.setText(adresse);



            boolean boolCB = item.getCarteBancaire() != null && item.getCarteBancaire();
            int visibleImageCarteBancaire = boolCB ? View.VISIBLE : View.GONE;
            imageCarteBancaire.setVisibility(visibleImageCarteBancaire);

            boolean boolGR = item.getCarteGR() != null && item.getCarteGR();
            int visibleImageCarteGR = boolGR ? View.VISIBLE : View.GONE;
            imageCarteGR.setVisibility(visibleImageCarteGR);

            boolean boolEspece = item.getEspece() != null && item.getEspece();
            int visibleImageEspece = boolEspece ? View.VISIBLE : View.GONE;
            imageEspece.setVisibility(visibleImageEspece);

            String nbrPlacesString = item.getNombrePlacesDisponibles() + " / " + item.getNombrePlacesTotal();
            nbrPlaces.setText(nbrPlacesString);


            // si c'est un favoris de l'utilisateur, on l'ajoute comme favoris
            if (item.getFavoris()) {
                iconFavoriteParking.setImageResource(R.mipmap.ic_haert_red_favorite);
            } else {
                iconFavoriteParking.setImageResource(R.mipmap.ic_haert_white_favorite);
            }
        }
        return actualView;
    }

}
